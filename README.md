### Notes
---
- Some personal, misc changes to Deathstrider.
- Doesn't change base Deathstrider (mechanically).
- Doesn't change your Deathstrider settings or do anything .ini related.
- Network compatible. Shouldn't desync if someone isn't running this.

### The Changes So Far
---
- Money bar no longer completely obscures the heartbeat indicator.
- Lowered player fist swing & fist impact volume.
- Lowered bullet casing volume.
- Lowered sizzling volume.
- No more moisture fog on the Spicy Air gas mask's lenses. And before you say "muh immersion"/"it's cheating", good gas masks have nasal seals. Make sure you're wearing them properly! That's what they're for, you silly geese.
- The hotbar is 100% transparent aside from the numbers.

### Credits
---
- UndeadRyker (me)
	- Basically all the changes mentioned above. :^)